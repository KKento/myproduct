﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateShipController : MonoBehaviour
{
    GameObject ship;

    public void CreateShip()
    {
        Instantiate(ship);
    }

}
