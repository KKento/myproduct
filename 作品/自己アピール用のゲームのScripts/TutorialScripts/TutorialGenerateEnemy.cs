﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialGenerateEnemy : MonoBehaviour
{
    [SerializeField, Header("TutorialController")] TutorialController tutorialController;
    [SerializeField, Header("PlayercamraRig")] GameObject playerCameraRig;
    [SerializeField, Header("スライム")] GameObject tutoSlime;
    [SerializeField, Header("タートル")] GameObject tutoTurtle;
    [SerializeField, Header("スライムエフェクト")] GameObject tutoSlimeEffect;
    [SerializeField, Header("タートルエフェクト")] GameObject tutoTurtleEffect;
    [SerializeField, Header("敵出現1")] GameObject tutoEnemySpawnPoint1;
    [SerializeField, Header("敵出現2")] GameObject tutoEnemySpawnPoint2;
    [SerializeField, Header("敵出現3")] GameObject tutoEnemySpawnPoint3;
    [SerializeField, Header("敵出現4")] GameObject tutoEnemySpawnPoint4;


    public void InstansEffect()
    {
        Instantiate(tutoSlimeEffect, tutoEnemySpawnPoint1.transform.position, Quaternion.identity);
        Instantiate(tutoTurtleEffect, tutoEnemySpawnPoint2.transform.position, Quaternion.identity);
        Instantiate(tutoSlimeEffect, tutoEnemySpawnPoint3.transform.position, Quaternion.identity);
        Instantiate(tutoSlimeEffect, tutoEnemySpawnPoint4.transform.position, Quaternion.identity);
        StartCoroutine("InstansEnemy");
    }


    IEnumerator InstansEnemy()
    {
        yield return new WaitForSeconds(0.2f);
        CreateEnemy(tutoSlime, tutoEnemySpawnPoint1);
        CreateEnemy(tutoTurtle, tutoEnemySpawnPoint2);
        CreateEnemy(tutoSlime, tutoEnemySpawnPoint3);
        CreateEnemy(tutoSlime, tutoEnemySpawnPoint4);

    }

    private void CreateEnemy(GameObject enemy,GameObject spawnPoint)
    {
        var ene = Instantiate(enemy, spawnPoint.transform.position, Quaternion.identity).GetComponent<TutorialEnemy>();
        //生成した敵にplayerCameraRigをアタッチ
        ene.SetTutorialContoller(tutorialController);
        //生成した敵にplayerCameraRigをアタッチ
        ene.SetCameraRig(playerCameraRig);
    }

}
