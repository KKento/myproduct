﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialController : MonoBehaviour
{
    [SerializeField, Header("RotateShipController")] RotateShipController rotateShipController;
    //今だけデバッグ用にpublic
    public int turtoEnemy;//チュートリアルの敵の数


    //チュートリアルの敵の数を把握
    public void OnTutoEnemyCount()
    {
        turtoEnemy++;
        Debug.Log(turtoEnemy);
    }


    //Tutorial敵が持ってるScriptから呼び出されるメソッド
    public void OnDeathTutoEnemy()
    {
        //敵が一体死ぬごとにカウントが減ってく
        turtoEnemy--;

        //カウントが0になったら船が動くようになる
        if (turtoEnemy <= 0)
        {
            rotateShipController.OnDamageJuge();
            //shipCreate();
        }
    }

}
