﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialEnemy : MonoBehaviour
{
    [SerializeField, Header("敵のHP")] int enemyHp;
    [SerializeField] TutorialController tutorialController;
    private GameObject player;

    public void SetTutorialContoller(TutorialController tutorialcontroller)
    {
        tutorialController = tutorialcontroller;
    }


    public void SetCameraRig(GameObject cameraRig)
    {
        player = cameraRig;
    }


    private void Start()
    {
        tutorialController.OnTutoEnemyCount();
        transform.LookAt(player.transform.position);
    }


    private void OnCollisionEnter(Collision other)
    {

        if (other.gameObject.tag == "Cannon Ball")
        {
            enemyHp--;
            if (enemyHp <= 0)
            {
                Debug.Log("On hit Cannon ball");
                thisDestroy();
            }

        }

        if (other.gameObject.tag == ("DropArea"))
        {
            Debug.Log("DropArea");

            Rigidbody rd;

            rd = this.GetComponent<Rigidbody>();
            rd.useGravity = true;
        }

        if (other.gameObject.tag == ("Break Point"))
        {
            thisDestroy();
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("DropArea"))
        {
            Debug.Log("当たった。");

            Rigidbody rd;

            rd = this.GetComponent<Rigidbody>();
            rd.useGravity = true;
        }
    }


    private void thisDestroy()
    {
        Debug.Log("ThisDestroy");
        tutorialController.OnDeathTutoEnemy();
        Destroy(this.gameObject);
    }
}
