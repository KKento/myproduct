﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDamageController : MonoBehaviour
{
    [SerializeField, Header("PlayerCon")] PlayerCon playerCon;
    private float damageTime;
    private float nowTime;


    // Start is called before the first frame update
    void Start()
    {
        damageTime = playerCon.DamageTime;
        nowTime = damageTime;
    }


    //敵が触れてる間を判断
    private void OnCollisionStay(Collision collision)
    {
        //layer Enemy,Enemy2が触れた場合
        if (collision.gameObject.layer == 16 || collision.gameObject.layer == 17)
        {
            //敵が触れてる時間を計測
            nowTime -= Time.deltaTime;

            if (nowTime <= 0)
            {
                playerCon.OnHitDamage();
                nowTime = damageTime;
            }
        }
    }
}
