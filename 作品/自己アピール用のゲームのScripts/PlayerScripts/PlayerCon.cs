﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerCon : MonoBehaviour
{
    //[SerializeField] GameObject playerDamageAreaObj;
    [SerializeField] int playerHp;
    [SerializeField, Header("ダメージを負う時間")] float damageTime;
    [SerializeField, Header("HP表示テキスト")] TextMeshProUGUI playerHpText;


    public float DamageTime { get { return damageTime; } }


    public void OnHitDamage()
    {
        playerHp--;
        Debug.Log("ダメージを負った、現在のHpは" + playerHp);
        NowPlayerHpText();
        //StartCoroutine("NowPlayerHpTextColor");//ダメージを負った際にHPテキストの色を変更
        NowPlayerHpTextColor(Color.red);
        Invoke("Delay", 1.0f);

        if (playerHp <= 0)
        {
            Debug.Log("ゲームオーバー");
            //ゲームオーバーメソッド
        }
    }


    private void Awake()
    {
        NowPlayerHpText();
       // playerDamageArea = playerDamageAreaObj.GetComponent<BoxCollider>();
    }


    // Start is called before the first frame update
    void Start()
    {

    }


    //PlayerHpのテキストを更新する
    private void NowPlayerHpText()
    {
        playerHpText.text = "HP "+playerHp;
    }


    private void Delay()
    {
        NowPlayerHpTextColor(Color.white);
    }


    private void NowPlayerHpTextColor(Color color)
    {
        playerHpText.color = color;
    }

    //private IEnumerator NowPlayerHpTextColor()
    //{
    //    playerHpText.color = Color.red;
    //    yield return new WaitForSeconds(1.0f);
    //    playerHpText.color = Color.white;
    //}

}
