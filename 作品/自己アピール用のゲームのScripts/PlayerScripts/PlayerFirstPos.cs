﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFirstPos : MonoBehaviour
{
    [SerializeField, Header("プレイヤーの初期値点")] Vector3 playerFirstPos;


    private void Awake()
    {
        this.transform.position = playerFirstPos;  //プレイヤーの初期値点を設定
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
