﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateShipStatus : MonoBehaviour
{
    [SerializeField, Header("船のHP")] int shipHp;//船のHP
    [SerializeField, Header("何ダメージ起きにフェイズ変わるか")] int phaseHp; //何ダメージ起きにフェイズ変わるか
    [SerializeField, Header("船の移動速度")] float shipSpeed;//船の移動速度
    [SerializeField, Header("船の沈む速度")] float sinkSpeed;//HPが0になった際の最後の動きの速度
    [SerializeField, Header("中心")] Vector3 rotateCenter;//どこを中心に回るか
    [SerializeField, Header("半径")] float radiusPoint;//中心から半径どれくらいを回るか
    [SerializeField, Header("船objの回転")] int shipRotate;//船objの回転(船のobjによっては正面が変わるため)


    public int Hp { get { return shipHp; } }
    public int PhaseHp { get { return phaseHp; } }
    public float ShipSpeed { get { return shipSpeed; } }
    public float SinkSpeed { get { return sinkSpeed; } }
    public Vector3 RotateCenter { get { return rotateCenter; } }
    public float RadiusPoint { get { return radiusPoint; } }
    public int ShipRotate { get { return shipRotate; } }
}

