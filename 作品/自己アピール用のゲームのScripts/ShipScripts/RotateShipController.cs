﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class RotateShipController : MonoBehaviour
{
    [SerializeField] RotateShipStatus rotateShipStatus;//ShipStatusからステータス呼び出し
    [SerializeField, Header("EnemyGenerate")] EnemyGenerate enemyGenerate;//EnemyGenerateのScript呼び出し

    //ShipStatusから呼び出す変数
    private int shipHp;//現在の船のHP
    private int phaseHp; //何ダメージ起きにフェイズ変わるか 
    private float shipSpeed;//船の移動速度
    private float sinkSpeed;//HPが0になった際の最後の動きの速度
    private Vector3 rotateCenter;//どこを中心に回るか
    private float radiusPoint;//中心から半径どれくらいを回るか
    private int shipRotate;//船objの回転(船のobjによっては正面が変わるため)

    //ShipStatusから呼び出さない変数
    private int phase = 1;//現在のフェイズと最初のフェイズ
    private int nowPhaseHp;//フェイズ管理用　現在何ダメージ食らったかk
    private float shipMoveTime;//time.deltimeで船を動かすための数字
    private bool damageJudge = false; //船がダメージを負うタイミング
    private bool shipStop = true;   //船が動けるか
    private float shipMoveX;//船の円回転の移動の際に必要
    private float shipMoveZ;//船の円回転の移動の際に必要


    public void OnDamageJuge()
    {
        shipStop = false;
        enemyGenerate.EnemySpawnStart();

        if (damageJudge == false)
        {
            damageJudge = true;
        }
    }


    //ShipStatusから参照用
    private void Awake()
    {
        shipHp = rotateShipStatus.Hp;
        phaseHp = rotateShipStatus.PhaseHp;
        shipSpeed = rotateShipStatus.ShipSpeed;
        sinkSpeed = rotateShipStatus.SinkSpeed;
        rotateCenter = rotateShipStatus.RotateCenter;
        radiusPoint = rotateShipStatus.RadiusPoint;
        shipRotate = rotateShipStatus.ShipRotate;
    }


    void Start()
    {
        nowPhaseHp = phaseHp; 
    }


    void Update()
    {
        if (shipStop == false)
        {
            shipMoveTime += Time.deltaTime;


            if (shipHp <= 0)//船のHPがなかったら直進しながら沈む
            {
                SinkShip(shipSpeed);
            }
            else
            {
                RotateShip();
            }
        }
    }
    

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Cannon Ball"))
        {
            ShipDamage();
        }
    }


    private void RotateShip()
    {
        transform.LookAt(Vector3.zero);
        this.transform.Rotate(0, shipRotate, 0);
        shipMoveX = radiusPoint * Mathf.Sin(shipMoveTime * shipSpeed);      //X軸の設定
        shipMoveZ = radiusPoint * Mathf.Cos(shipMoveTime * shipSpeed);      //Z軸の設定

        transform.position = new Vector3(shipMoveX + rotateCenter.x, rotateCenter.y, shipMoveZ + rotateCenter.z);
    }


    //船が沈み中,HPが0になったら呼ばれる
    private void SinkShip(float TF)//TF == true or false 船が動いているか止まっているか　移動速度or0
    {
        Debug.Log("沈み中");
        gameObject.transform.Translate(0, -sinkSpeed, TF);
        StartCoroutine("Destroy");//3s後に自壊するコルーチン呼び出し
    }


    private void ShipDamage()
    {
        if (damageJudge == true)
        {
            shipHp--;
            if (shipHp > 1)
            {
                nowPhaseHp--;
                if (nowPhaseHp == 0)
                {
                    phase++;
                    enemyGenerate.NowPhase(phase);
                    nowPhaseHp = phaseHp;
                }
            }
            else if (shipHp <= 0)
            {
                //98行目に記載
                /*
                Rigidbody rd;

                rd = this.GetComponent<Rigidbody>();
                rd.useGravity = true;
                Invoke("InvokeDestroy", 3.0f);
                */
            }
        }
    }


    private IEnumerator Destroy()
    {
        yield return new WaitForSeconds(3.0f);
        Destroy(gameObject);
    }
}
