﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonController : MonoBehaviour
{
    //警告文を出さないように
#pragma warning disable 649
    [SerializeField] GameObject cannonball;
    //弾を保持（プーリング）する空のオブジェクト
    //Transform bullets;


    //弾が生成されるポジションを保有するゲームオブジェクト
    [SerializeField] Transform batteryposition;

    private float fire;
    [SerializeField] float fireCt;

    [SerializeField, Header("右手")] bool rightctr;
    [SerializeField, Header("左手")] bool leftctr;

    //弾のスピード
    [SerializeField] float speed;

    [SerializeField] ParticleSystem smoke;
#pragma warning restore 649

    AudioSource bombSE;
    private void Awake()
    {
        bombSE = gameObject.GetComponent<AudioSource>();
        //弾を保持する空のオブジェクトを生成
        //bullets = new GameObject("PlayerBullets").transform;
    }

    // Start is called before the first frame update
    void Start()
    {
        fire = 0;
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(fire);
        if (fire >= 0)
        {
            fire -= Time.deltaTime;
        }

        //スペースキーで発射
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (fire <= 0)
            {
                OnFire();

                fire = fireCt;
            }
        }
        

        //左手のコントローラー
        if (leftctr == true && fire <= 0)
        {
            if (OVRInput.Get(OVRInput.RawButton.LIndexTrigger))
            {
                OnFire();

                fire = fireCt;
            }
        }

        //右手のコントローラー
        if (rightctr == true && fire <= 0)
        {
            if (OVRInput.Get(OVRInput.RawButton.RIndexTrigger))
            {
                OnFire();

                fire = fireCt;

            }
        }
    }


    public void OnFire()
    {
        Debug.Log("Fire in the Hole!!");  // ログを出力
        bombSE.Play();

        /*
                //アクティブでないオブジェクトをbulletsの中から探索
        foreach(Transform t in bullets)
        {
            if (!t.gameObject.activeSelf)
            {
                //非アクティブなオブジェクトの位置と回転を設定
                t.SetPositionAndRotation(pos, rotation);
                //アクティブにする
                t.gameObject.SetActive(true);
                return;
            }
        }
        //非アクティブなオブジェクトがない場合新規生成

        //生成時にbulletsの子オブジェクトにする
        Instantiate(bullet, pos, rotation, bullets);
        */


        //ballをインスタンス化して発射
        GameObject createdBullet = Instantiate(cannonball) as GameObject;
        createdBullet.transform.position = batteryposition.transform.position;

        smoke.Play();
        //ParticleSystem createSmoke = Instantiate(smoke) as ParticleSystem;
        //createSmoke.transform.position = batteryposition.transform.position;

        //発射ベクトル
        Vector3 force;
        //発射の向きと速度を決定
        force = batteryposition.transform.forward * speed;
        // Rigidbodyに力を加えて発射
        createdBullet.GetComponent<Rigidbody>().AddForce(force);
    }
}
