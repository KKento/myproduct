﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonBallController : MonoBehaviour
{
    [SerializeField,Header("爆発の火力")] float pow;
    [SerializeField,Header("爆発の範囲")] float radius;
    [SerializeField,Header("爆発のエフェクト")] GameObject bomEffect;
    [SerializeField,Header("爆発の判定にひっかかるもの")] LayerMask layerMask;
    

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Ship"))
        {
            Vector3 pos = this.transform.position;
            BomEffect(pos);

            Debug.Log("船に当たったよ");
            Destroy(this.gameObject);
        }

        if (other.CompareTag("Break Point"))
        {
            Destroy(this.gameObject);
        }
    }


    private void OnCollisionEnter(Collision other)
    {
        AddForce();
    }


    private void AddForce()
    {
        //物に当たった時の弾の位置を取得
        Transform myTransform = this.transform;
        Vector3 pos = myTransform.position;

        //上記で取得した位置の範囲内にある敵objを取得して配列に入れる
        //int layerMask = LayerMask.GetMask(new string[] { "Enemy"});
        //Collider[] cols = Physics.OverlapSphere(pos, radius, -1 - (1 << 14 | 1 << 17));
        Collider[] cols = Physics.OverlapSphere(pos, radius, layerMask);
        foreach (Collider target in cols)
        {

            //範囲内のゲームオブジェクト全てのRigidbodyにAddExplosionForceする
            if (target.GetComponent<Rigidbody>() != null)
            {
                target.GetComponent<Rigidbody>().AddExplosionForce(pow, pos, radius);
                var enemy = target.GetComponent<RotateEnemyController>();
                if (enemy != null) 
                {
                    enemy.NotMove();
                }
            }
        }

        BomEffect(pos);
        Destroy(this.gameObject);
    }


    private void BomEffect(Vector3 pos)
    {
        //爆破パーティクル
        Instantiate(bomEffect, pos, Quaternion.identity);
    }

}
