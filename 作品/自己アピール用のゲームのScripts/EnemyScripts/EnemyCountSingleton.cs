﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCountSingleton 
{
    private static EnemyCountSingleton _instance;
    private int enemyCount;



    public static EnemyCountSingleton Instance
    {
        get
        {
            if (_instance == null) _instance = new EnemyCountSingleton();
            return _instance;
        }
    }

    public void Enemyplus ()
    {
        enemyCount++;
        Debug.Log("エネミー増えて" + enemyCount);
    }

    public void EnemyMinus()
    {
        enemyCount--;
        Debug.Log("エネミー減って" + enemyCount);
    }
}
