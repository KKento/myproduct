﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGenerate : MonoBehaviour
{
    [SerializeField, Header("敵生成1")] GameObject eneGene1Obj; //敵生成場所の1～5
    [SerializeField, Header("敵生成2")] GameObject eneGene2Obj;
    [SerializeField, Header("敵生成3")] GameObject eneGene3Obj;
    [SerializeField, Header("敵生成4")] GameObject eneGene4Obj;
    [SerializeField, Header("敵生成5")] GameObject eneGene5Obj;
    [SerializeField, Header("スライムobj")] GameObject slime; //敵エネミー１
    [SerializeField, Header("タートルobj")] GameObject turtle; //敵エネミー2
    [SerializeField, Header("スライム生成エフェクト")] GameObject slimeEffect; //エネミー1の出現エフェクト
    [SerializeField, Header("タートル生成エフェクト")] GameObject turtleEffect; //エネミー2の出現エフェクト

    [SerializeField, Header("敵生成時間")] float enemySpawnTime; //敵が出現するまでのリセット用時間
    private float nowTime; //敵が出現するまでの残り時間
    private int randomValue;  //敵生成のランダムで使う
    [SerializeField, Header("PlayerCamerarig")] GameObject playerCameraRig; //敵生成する際にプレイヤーの方向向くためのobj
    private bool spawnStop = true; //敵出現のon,off
    private int phase = 1;//現在のフェイズ

    //Serializetableで二次元配列化したかった
    //敵出現の敵の種類と場所
    /*
    [Serializable]
    public class EnemySpawnConreoller
    {
        [SerializeField, Header("敵の沸く場所")] public GameObject enemySpawn;
        [SerializeField, Header("敵の種類")] public GameObject enemy;
    }
    */


    //RotateShipControllerから現在のphaseを呼び出し管理するためのメソッド
    public void NowPhase(int shipPhase)
    {
        Debug.Log("現在のフェイズ" + phase);
        phase = shipPhase;
    }


    //RotateShipControllerからチュートリアルの敵が死ぬまで敵生成ストップ
    public void EnemySpawnStart()
    {
        spawnStop = false;
    }


    void Start()
    {
        nowTime = enemySpawnTime;
    }


    private void Update()
    {
        if (spawnStop == false)
        {
            nowTime -= Time.deltaTime;
            if (nowTime <= 0)
            {
                EnemyGene();
                nowTime = enemySpawnTime;
            }
        }
    }


    void EnemyGene()
    {
        switch (phase)
        {
            case 1://スライム1体出現

                EnemyInstantiate(slime, eneGene1Obj, slimeEffect);
                break;


            case 2://スライム2体出現

                EnemyInstantiate(slime, eneGene1Obj, slimeEffect);
                EnemyInstantiate(slime, eneGene2Obj, slimeEffect);

                nowTime = enemySpawnTime;

                break;


            case 3://スライム1体とタートル1体

                randomValue = UnityEngine.Random.Range(1, 5);

                EnemyInstantiate(turtle, eneGene1Obj, turtleEffect);
                EnemyInstantiate(slime, eneGene2Obj, slimeEffect);

                nowTime = enemySpawnTime;

                break;


            case 4://スライム4体&ランダムで1地点にタートル
                
                randomValue = UnityEngine.Random.Range(1, 5);
                //Rondomで1がでたら1posにタートル出現
                if (randomValue == 1)
                {
                    EnemyInstantiate(turtle, eneGene1Obj, turtleEffect);
                }
                else
                {
                    EnemyInstantiate(slime, eneGene1Obj, slimeEffect);
                }

                EnemyInstantiate(slime, eneGene2Obj, slimeEffect);
                EnemyInstantiate(slime, eneGene3Obj, slimeEffect);
                EnemyInstantiate(slime, eneGene4Obj, slimeEffect);
                EnemyInstantiate(slime, eneGene5Obj, slimeEffect);

                nowTime = enemySpawnTime;

                break;


            case 5://スライム4体とランダムの場所に1体タートル //randomの場所が4だったら4の場所に出す
                EneGeneCase5();


                nowTime = enemySpawnTime;

                break;
        }

        //Stateで管理したかった
        /*
        var enemyGeneState = new Case1();
        EnemyGeneStete.
        */
    }


    private void EneGeneCase5()
    {
        randomValue = UnityEngine.Random.Range(1, 5);

        switch (randomValue)
        {
            case 1:

                EnemyInstantiate(turtle, eneGene1Obj, turtleEffect);
                EnemyInstantiate(slime, eneGene2Obj, slimeEffect);
                EnemyInstantiate(slime, eneGene3Obj, slimeEffect);
                EnemyInstantiate(slime, eneGene4Obj, slimeEffect);
                EnemyInstantiate(slime, eneGene5Obj, slimeEffect);

                break;

            case 2:

                EnemyInstantiate(slime, eneGene1Obj, slimeEffect);
                EnemyInstantiate(turtle, eneGene2Obj, turtleEffect);
                EnemyInstantiate(slime, eneGene3Obj, slimeEffect);
                EnemyInstantiate(slime, eneGene4Obj, slimeEffect);
                EnemyInstantiate(slime, eneGene5Obj, slimeEffect);

                break;

            case 3:

                EnemyInstantiate(slime, eneGene1Obj, slimeEffect);
                EnemyInstantiate(slime, eneGene2Obj, slimeEffect);
                EnemyInstantiate(turtle, eneGene3Obj, turtleEffect);
                EnemyInstantiate(slime, eneGene4Obj, slimeEffect);
                EnemyInstantiate(slime, eneGene5Obj, slimeEffect);

                break;

            case 4:
                EnemyInstantiate(slime, eneGene1Obj, slimeEffect);
                EnemyInstantiate(slime, eneGene2Obj, slimeEffect);
                EnemyInstantiate(slime, eneGene3Obj, slimeEffect);
                EnemyInstantiate(turtle, eneGene4Obj, turtleEffect);
                EnemyInstantiate(slime, eneGene5Obj, slimeEffect);

                break;

            case 5:
                EnemyInstantiate(slime, eneGene1Obj, slimeEffect);
                EnemyInstantiate(slime, eneGene2Obj, slimeEffect);
                EnemyInstantiate(slime, eneGene3Obj, slimeEffect);
                EnemyInstantiate(slime, eneGene4Obj, slimeEffect);
                EnemyInstantiate(turtle, eneGene5Obj, turtleEffect);

                break;
        }
    }


    void EnemyInstantiate(GameObject enemy, GameObject spawnPoint, GameObject spawnEffect)
    {
        //敵をカウント用のシングルトンにプラス1
        EnemyCountSingleton.Instance.Enemyplus();
        //敵生成する場所にエフェクト生成
        Instantiate(spawnEffect, spawnPoint.transform.position, Quaternion.identity);
        var ene = Instantiate(enemy, spawnPoint.transform.position, Quaternion.identity).GetComponent<RotateEnemyController>();
        //生成した敵にplayerCameraRigをアタッチ
        ene.SetCameraRig(playerCameraRig);
        //敵生成したら敵生成時間をリセット
        nowTime = enemySpawnTime;
    }

}