﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateEnemyController : MonoBehaviour
{
    [SerializeField, Header("敵の移動速度")] float enemySpeed;//敵の移動スピード
    [SerializeField, Header("敵のHP")] int enemyHp;//敵のHP
    GameObject player;//Playerを取得して敵が近づくため使用
    public bool move = true;//敵の移動のon,off


    //EnemyGenerateでインスタンスした瞬間に呼び出されるメソッド
    public void SetCameraRig(GameObject cameraRig) 
    {
        player = cameraRig;
    }


    // Update is called once per frame
    void Update()
    {
        //プレイヤーの方向を向いて近づいてくる
        transform.LookAt(player.transform.position);

        if (move == true)
        {
            Move();
        }
    }


    void Move()
    {
        transform.position = Vector3.MoveTowards(transform.position, player.transform.position, enemySpeed);
    }


    //敵が爆風に当たった時に抵抗しないように移動をoffにする
    public void NotMove()
    {
        move = false;
    }


    private void OnCollisionEnter(Collision other)
    {

        if (other.gameObject.tag == "Cannon Ball")
        {
            enemyHp--;
            if(enemyHp == 0)
            {
                ThisDestroy();
            }
        }

        if (other.gameObject.tag == ("DropArea"))
        {
            Debug.Log("DropArea");

            Rigidbody rd;

            rd = this.GetComponent<Rigidbody>();
            rd.useGravity = true;
        }

        //落下してBreak Pointに当たるとやられる
        if (other.gameObject.tag == ("Break Point"))
        {
            ThisDestroy();
        }
    }


    //吹き飛ばされてDropAreaに当たるとRigidBody働いて落下する
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("DropArea")){
            Debug.Log("当たった。");

            Rigidbody rd;

            rd = this.GetComponent<Rigidbody>();
            rd.useGravity = true;
        }
    }


    //敵がやられたときに呼び出す
    private void ThisDestroy()
    {
        //敵をカウント用のシングルトンにマイナス1
        EnemyCountSingleton.Instance.EnemyMinus();
        Destroy(this.gameObject);
    }
}
