﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleController : MonoBehaviour
{
    [SerializeField, Header("TutotialGenerateEnemy")] TutorialGenerateEnemy tutorialGenerateEnemy;

    //[SerializeField, Header("イージーキューブ")] GameObject easyCube;
    //[SerializeField, Header("ノーマルキューブ")] GameObject normalCube;
    //[SerializeField, Header("ハードキューブ")] GameObject hardCube;

    [SerializeField, Header("スタートUI")] UICube startCube;
    [SerializeField, Header("終了キューブUI")] UICube endCube;


    // Start is called before the first frame update
    void Start()
    {
        startCube.SetAction(GameStart);
        endCube.SetAction(GameExit);
    }


    //UICubeにて使用
    private void GameStart() 
    {
        Debug.Log("スタート");
        tutorialGenerateEnemy.InstansEffect();
        Destroy(this.gameObject);
    }


    private void GameExit()
    {
        Debug.Log("終了");
        //終了処理
    }
}
