﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UICube : MonoBehaviour
{
    //タイトルのcannon ballに当たるオブジェクトすべてにアタッチする。
    //TitleControllerのものと同期させる

   // [SerializeField, Header("OVERCameraRig")] GameObject player;


    System.Action _onHit;

    private void Start()
    {
        //transform.LookAt(player.transform.position);
    }




    public void SetAction(System.Action onHit) 
    {
        _onHit = onHit;
    }


    //nullチェック
    private void OnCollisionEnter(Collision collision)
    {
        if (_onHit != null) {
            _onHit();
        }

    }
}
