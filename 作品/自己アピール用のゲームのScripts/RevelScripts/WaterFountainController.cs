﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterFountainController : MonoBehaviour
{
    public GameObject effect;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }



    private void OnTriggerEnter(Collider other)
    {


        //物に当たった時の弾と敵の位置を取得
        Transform Transform = other.transform;
        Vector3 pos = Transform.position;

        //爆破パーティクル
        Instantiate(effect, pos, Quaternion.identity);

        Debug.Log("水しぶき");
    }

}
